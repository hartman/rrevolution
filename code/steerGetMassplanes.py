'''
Loop over the years and pairing algos of interest to collect 
the corresponding massplanes.
'''

import os

prodTag = 'FEB20'
pTcut = 40

#for physicsSample in ['data16','data17','data18']:
for physicsSample in ['data17']:

	for pairing,pconfig in zip(['MDR','min_dR1','pag','pag'], ['x','x','SM_2b','kappa_10']):

		nSelectedJets = 4 if 'R' in pairing else 5

		pythonCmd =  f"python getMassplanes.py --physicsSample {physicsSample} "
		pythonCmd += f"--prodTag {prodTag} --nSelectedJets {nSelectedJets} --pTcut {pTcut} "
		pythonCmd += f"--pairing {pairing} --pairAGraphConfig {pconfig} "
		
		job = f'massplanes_{physicsSample}_{pairing}_{pconfig}'
		bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0026]\' -W 4:00 -oo log_files/{job}.txt -J {job} {pythonCmd}"
		os.system(bsubCmd) 

