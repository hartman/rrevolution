'''
Implements the resolved analysis cutflow.

This file runs in 2 modes:
(1) Run over individual analysis cutflow for each MNT
(2) Concat

Updated: Mar 2023

'''
import uproot


from glob import glob
from time import time
from itertools import product

import numpy as np
import pandas as pd
import yaml, json
import os

import operator
import dask
import dask.dataframe as dd
from dask.distributed import Client
import pyarrow as pa
import pyarrow.parquet as pq

dataDir = '../../data'
from utils import fileDirSLAC as fileDir #, getFileDirSLAC as getFileDir
from utils import getSubDir, mcToYr, L, read_tsv
from utils import physToDSID, get_xsec, stdCols, truthCols, getJetCols
from analysis import pairAndProcess

def getLambdaWeights(smnr,
                     lambdaFile='../data/weight-mHH-from-cHHHp01d0-to-cHHHpx_10GeV_Jul28.root',
                     w_sm='mc_sf', returnWeight=False):
    '''
    Add weights corresponding to the different lambda variations to the df
    - smnr: pandas df
    - lambdaFile: The file storing the reweighting histogram for truth_mhh
    '''

    f = uproot.open(lambdaFile)

    for kl in np.arange(-20,21):

        if kl == 1: # no need to rw
            continue

        neg = 'n' if kl < 0 else ''
        histName = f'reweight_mHH_1p0_to_{neg}{np.abs(kl)}p0'

        h = f[histName].to_hist()
        rw, all_edg = h.to_numpy(flow=True)
        edg = all_edg[1:-1]

        idx = np.digitize(smnr['truth_mhh'],edg)
        ws = get_xsec(kl)/get_xsec(1) * rw[idx] * smnr['mc_sf']
        
        if returnWeight:
            return ws
        else:
            smnr[f'w_k{kl}'] = ws

def getSampleWeight(files, dsid, lumi=1):
    '''
    Get the weight normalization to apply to mc_sf.
    '''
    sum_weights_initial = 0
    # meta_name = f"CutBookkeeper_{dsid}_*_NOSYS" 

    for fname in files:
        # with uproot.open(f'{fname}:{meta_name}') as meta_hist:
        meta_hist_list = []
        with uproot.open(fname) as f:
            for k in f.keys():
                if f'CutBookkeeper_{dsid}' in k:
                    meta_hist_list.append(k)

            assert len(meta_hist_list) == 1 # make sure we only found one file match
            
            meta_hist = f[meta_hist_list[0]]

        h,e = meta_hist.to_numpy()
        sum_weights_initial += h[1]

    print("sum_weights_initial",sum_weights_initial)

    db_entry = read_tsv()

    if (dsid > 801577):
        print('Concat X->SH->4b, norm to 0.1 pb (instead of the ami xsec)')
        xsec = 0.1 # pb 
    else:
        xsec = db_entry.loc[dsid,"xsec"]


    print(f'Reading entry for DSID {dsid}:')
    sample_weight = lumi
    sample_weight *= xsec
    sample_weight *= db_entry.loc[dsid,"k_factor"]
    sample_weight *= db_entry.loc[dsid,"gen_filter_eff"]
    sample_weight /= sum_weights_initial

    print("  xsec",xsec)
    print("  k_factor",db_entry.loc[dsid,"k_factor"])
    print("  gen_filter_eff",db_entry.loc[dsid,"gen_filter_eff"])
    print("  sample_weight",sample_weight)

    return sample_weight

def normalizeWeight(df,files,lumi,i_mc=450000):
    '''
    Load in the MNT to get the relevant normalization from the sample_weight and lumi
    Inputs:
    - df: If a df is passed: normalize to mc_sf (in place).
          If `None`, Otherwise, the sample_weight will be returned (as a scalar)
    - physicsSample: key to retrive the correct MDT from the fileDir dictionary
    - lumi: Luminosity to normalize the sample to
    - i_mc: DSID to read in the xsec, k_factor, and generator filter eff from
            xsec.tsv file.
    '''

    # files = glob(fileDir[key] + "user.*.output-hh4b.root")
    sample_weight = getSampleWeight(files, i_mc, lumi)

    if df is None:
        return sample_weight

    df['mc_sf'] *= sample_weight


def getNumMNTs(MNT_list,subDir,fileTag):
    '''
    Goal: For the sanity check in my assertion statement, calulate the # of MNTs
    that we can account for from
    1. No events passing the trigger + 4 good jets cuts
    2. No XhhMiniNtuple tree in the MNT file

    '''

    numMNTs = len(MNT_list)

    # Loop throught the MNT files, and if we can *explain* why there isn't a
    # corresponding output .parquet file, subtract from the numMNTs count
    for fname in MNT_list:
    
        # Get tags for the (1) outputFile and (2) cutFlowFile
        fileNumTag = fname.split('.')[-3]
    
        config = f'{fileTag}{fileNumTag}'
    
        outputFile = f'{dataDir}/{subDir}/files/df{config}.parquet'
    
        if os.path.exists(outputFile): 
            continue

        # Check if the MNT file had the tree
        tname = 'AnalysisMiniTree'
        t = uproot.open(f'{fname}:{tname}')
        if t.num_entries == 0:
            print(f'{fname} doesn\'t have any events in the MNT tree')
            numMNTs -= 1
        else:
            print(f'ISSUE: We don\'t know why {fname} wasn\'t processed')

    return numMNTs

def concatDfs(subDir,key,fileTag,physicsSample,year):
    '''
    Goal: Conatenate the dfs in each year and save the corresponding file

    Inputs:
    - subDir: Where the output files got *stored*
    - key: For retrieving the MNT normalization using the fileDir MNT loc dictionary
    - fileTag: Specifics of the name w/ pairing, # tags, and jet sort
    - physicsSample: For the mapping to the DSID entry in read_tsv
    - year: For the luminosity normalization
    '''

    if ('X' in key) and ('S' in key):
        print(key)
        keys = key.split('-')

        for i,ki in enumerate(keys):
            print(i,ki)

        prodTag = keys[-1]

        vals = keys[0].split('_')
        mc = vals[-1]
        physicsSample = '_'.join(vals[:-1])

        print(physicsSample,mc,prodTag)

        fDir = getFileDir(physicsSample,mc,prodTag)
    else:
        fDir = fileDir[key]
    wildFile = fDir+"*.root"
    MNT_list = glob(wildFile)
    numMNTs = getNumMNTs(MNT_list,subDir,fileTag) 
    print(numMNTs)

    tag = f'_{ntag}b' if ntag != -1 else ''
    if 'data' in physicsSample:
        fname = f"{dataDir}/{subDir}/files/df{fileTag}_period?_00*.parquet"
    else:
        fname = f"{dataDir}/{subDir}/files/df{fileTag}_00*.parquet"
    print('fname',fname)
    numPQ = len(glob(fname))

    if numMNTs != numPQ:
        # print(f'Error: {numMNTs} MNT, but only {numPQ} processed... returning')
        # return
        print(f'WARNING: {numMNTs} MNT, but only {numPQ} processed... will still concat')
        
    dask.config.set(scheduler='single-threaded')

    f_all = glob(fname)
    f_valid = [f for f in f_all if len(pd.read_parquet(f)) > 0]

    dat = dd.read_parquet(f_valid) 
    df = dat.compute()

    # Normalize to the corresponding year's luminosity
    if 'data' not in physicsSample:
        print(key)
        lumi = L[year]
        dsid = physToDSID[physicsSample]

        normalizeWeight(df,MNT_list,lumi,dsid)

    # Step 4: kappa lambda reweighting
    if 'SMNR' in physicsSample:
        getLambdaWeights(df)

    filename = f"{dataDir}/{subDir}/df{fileTag}{tag}.parquet"
    table  = pa.Table.from_pandas(df)
    pq.write_table(table,filename)


if __name__ == '__main__':

    import argparse

    p = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    p.add_argument('--physicsSample', type=str, default='SMNR',
                   help="The physics sample to run over (default SMNR (STILL NEED TO PRODUCE))")
    p.add_argument('--mc', type=str, default='mc20e',
                    help="The mc campaign to consider (default mc20e)")
    p.add_argument('--prodTag', type=str, default='FEB24',
                   help="The production tag for the miniNtuple (default FEB24).")

    p.add_argument('--batch', action='store_true',
                   help="Submit the individual jobs on the (slac) batch cluster")
    p.add_argument('--overwrite',action='store_true',
                   help='Resubmit the batch jobs, even if the output files already exist.')
    p.add_argument('--concat',action='store_true',help='concatenate the individual files')
    p.add_argument('--filename',type=str,default='',help='MNT file to run pairAndProcess() over')
    
    p.add_argument('--nSelectedJets',type=int,default=4,
                   help="Number of jets to consider for pairing (default 4, but can be larger for pairAGraph)")
    p.add_argument('--pTcut',type=int,default=40,help="pT cut to use for the jets (default 40 GeV)")
    p.add_argument('--ntag',type=int,default=4,help='ntag cut to apply to the outputs')
    p.add_argument('--tagger',type=str,default='DL1d')
    p.add_argument('--version',type=str,default='01',help='FTAG tagger version (default 01)')
    p.add_argument('--WP',type=int,default=77)

    p.add_argument('--pairing',type=str,default='min_dR',
                   help="Pairing algo to use: one of min_dR (default), or 'min_dH'")

    args = p.parse_args()

    physicsSample, prodTag = args.physicsSample, args.prodTag
    nSelectedJets, pTcut, batch = args.nSelectedJets, args.pTcut, args.batch
    filename, mc, ntag = args.filename, args.mc, args.ntag

    tagger, version, WP = args.tagger, args.version, args.WP

    pairing = args.pairing 
    assert (pairing == 'min_dR') 

    subDir = getSubDir(physicsSample,mc,prodTag,nSelectedJets,pTcut)
    key = '-'.join(subDir.split('-')[:2])

    ntag_str = f'_{ntag}b_{tagger}{WP}' 

    if not os.path.exists(f'{dataDir}/{subDir}'):
        os.mkdir(f'{dataDir}/{subDir}')
    if not os.path.exists(f'{dataDir}/{subDir}/files'):
        os.mkdir(f'{dataDir}/{subDir}/files')
        
    fileTag = f'_{pairing}{ntag_str}'

    if len(args.filename) > 0:

        fileNumTag = args.filename.split('.')[-3]

        kwargs = {'inputFile': args.filename,
                  'ntag': ntag,
                  'tagger': tagger,
                  'ftag_version': version,
                  'WP': WP,
                  'pairing': pairing,
                  'cols': stdCols,
                  'physicsSample': physicsSample,
                  'nSelectedJets': nSelectedJets,
                  'pT_min': pTcut,
                  'fileTag': fileTag+fileNumTag,
                  'outputDir': f'{dataDir}/{subDir}',
                  'save':True}

        if 'data' in physicsSample:
            kwargs['physicsSample'] = physicsSample
            kwargs['truth'] = False

        else:
            kwargs['physicsSample'] = f'{physicsSample}_{mc}'
            kwargs['truth'] = False

            # if (('ttbar' in physicsSample) or ('JZ' in physicsSample)):
            #     kwargs['truth'] = False
            # else:
            #     kwargs['truth'] = True
            #     kwargs['cols'] += truthCols

        # Append some jet features
        kwargs['cols'] += getJetCols(nSelectedJets=nSelectedJets)

        pairAndProcess(**kwargs)

    elif args.concat:

        print(f"\n\nConcatenating files for {key} and {nSelectedJets}-jets with {ntag}b for {pairing}.")
        start = time()
        concatDfs(subDir, key, fileTag, physicsSample, mcToYr[mc])
        stop = time()

        run = stop - start
        print(f'Run time: {run // 60:.0f} min {run % 60:.0f} s')

    else:

        if ('X' in physicsSample) and ('S' in physicsSample):
            fDir = getFileDir(physicsSample,mc,prodTag)
        else:
            fDir = fileDir[key]
        wildFile = fDir+"*.root"
        print(wildFile)
        print(len(glob(wildFile)))

        for filename in glob(wildFile):

            print(filename)
            # There has to be a way to condense this...
            pythonCmd = f'python processMNTs.py --filename {filename} '
            for arg in vars(args):
                if (arg == 'filename'): continue
                val = getattr(args, arg)
                if val == False:
                    continue
                elif val == True:
                    pythonCmd += f'--{arg} '
                else:
                    pythonCmd += f'--{arg} {val} '

            fileNumTag = filename.split('.')[-3]
            outputFile = f'{dataDir}/{subDir}/files/df{fileTag}{fileNumTag}.parquet'
            job = f'{physicsSample}_{mc}{fileTag}{fileNumTag}'

            if args.batch:
                bsubCmd = f"bsub -R \'select[centos7 && hname != kiso0051]\' -W 120 -q medium -oo log_files/{job}.txt -J {job} {pythonCmd}"

                if (not args.overwrite) and (os.path.exists(outputFile)):
                    print(f'Not submitting: {outputFile} already exists')
                    continue
                os.system(bsubCmd)
            else:

                if (not args.overwrite) and (os.path.exists(outputFile)):
                    print(f'Not submitting: {outputFile} already exists')
                    continue
                print(pythonCmd)

                os.system(pythonCmd)

