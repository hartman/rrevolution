'''
Pairing MVA
'''

def mva(df,pconfig, tag='test',num_folds=2,HC_ordering='scalar_pt'):
    '''
    Takes care of some of the logic common to all of my pairing algos, i.e,
    applying the preprocessing to the inputs, loading in the model, and looping
    through the K folds.
    '''

    os.sys.path.append(f'{rrDir}/pairingMVAs/')
    from transformers import pairAGraph

    # Step 0: Load in the model config file
    with open(pconfig, 'r') as yaml_file:
        configDict = yaml.load(yaml_file, Loader=yaml.FullLoader)

    pairing = configDict['model']
    train = configDict['dataset']
    modelConfig = configDict['modelParams']
    tag = pconfig.split('/')[-1].split('.')[0]

    # Step 1: Do the ml dataset preprocessing
    physicsSample, mc, prodTag = train['physicsSample'], train['mc'], train['prodTag']
    jVars, nSelectedJets, sort = train['jetVars'], train['nSelectedJets'], train['sort']
    subDir = getSubDir(physicsSample,mc,prodTag,nSelectedJets)

    scalingFile = f"{rrDir}/configs/scale_{physicsSample}_{mc}-{prodTag}-{nSelectedJets}jets_{sort}_sort.json"
    df = transformData(df,jVars,scalingFile)

    # Step 2: Apply pairAGraph
    model = pairAGraph(inpt_dim=len(jVars),njets=nSelectedJets,**modelConfig)

    # New cols that we'll save the jet indices for each of the HCs in
    idxCols = [f'idx_h{hci}_j{ji}' for hci,ji in product(['a','b'], [1,2])]
    for c in idxCols: df[c] = 0

    # Load the trained weights
    for k in range(num_folds):

        modelName = f'{rrDir}/pairingMVAs/models/{subDir}/{tag}_{k+1}of{num_folds}/model.pt'
        print(modelName)
        state_dict = torch.load(modelName,map_location='cpu')
        model.load_state_dict(state_dict)
        model.eval()

        # Get the HCs
        mask  = (df.eventNumber % num_folds) == k # data not seen in training this model
        getHCs(df,mask,model,jVars,nSelectedJets)

    # Sort the HCs and the jets
    HC1,HC2,ja1,ja2,jb1,jb2 = sortHCs(df,nSelectedJets,HC_ordering)

    return HC1,HC2,ja1,ja2,jb1,jb2

