'''
Some vbf specific functions useful for HH4b
'''

# Before I used this to get the idx for the four jets in processDf
print('Getting the VBF selection')
pass_vbf_sel, idx = get_vbf_sel(t, evts, jarr)

pass_vbf_sel = pass_vbf_sel[four_jets]
idx = idx[four_jets]

evts.loc[four_jets.to_numpy(),'pass_vbf_sel'] = pass_vbf_sel.to_numpy()

def get_vbf_sel(t, evts,jarr):
    '''
    Get the vbf event selection.

    Inputs:
    - t: TTree (read by uproot)
    - evts: df w/ event level variables
    - jarr: Awkard array with the jet level features
    - four_jets: The trigger + 4 jets, 2 b-tags cuts
    
    Outputs:
    - pass_vbf_sel
    - idx: The indices where the first 4 correspond to the HC jets
    '''

    # Get the VBF jets
    farr = t.arrays( jcols, aliases=jalias, cut=f'(pt > 30) & (abs(eta) > 2.5) & (abs(eta) < 4.5)' )
    #farr = farr[four_jets]

    j_all = ak.concatenate([jarr, farr], axis=1)

    # Form 4-vectors s.t. we can take the Cartesian product
    v4 = ak.zip( { k: j_all[v] for k,v in vecDict.items() }, with_name="PtEtaPhiELorentzVector" )

    # For the event to be VBF, need at least 6 jets, 2 of which are anti-tagged
    evts['njets_all'] = ak.num(j_all.pt).to_numpy()
    num_untag = ak.sum(j_all.tag==0,axis=1)

    mask = (evts['njets_all'] >=6) & (num_untag>=2)

    jet_pairs = ak.combinations(   ak.mask(v4,mask),2)
    idx_pairs = ak.argcombinations(ak.mask(v4,mask),2)

    j1, j2 = ak.unzip(jet_pairs)
    i1, i2 = ak.unzip(idx_pairs)

    untagged_mask = (ak.mask(j_all['tag'],mask)[i1]==0) & (ak.mask(j_all['tag'],mask)[i2]==0)

    mjj_cand = ak.where(untagged_mask, (j1+j2).mass, 0)

    mjj = ak.argsort(mjj_cand,ascending=False)[:,:1]
    mjj_idx = ak.argsort(mjj_cand,ascending=False)[:,:1]

    # To *mask* when getting the HC jets (if the event passes the HC jet selection)
    vbf_jet_1 = ak.firsts(j1[mjj_idx])
    vbf_jet_2 = ak.firsts(j2[mjj_idx])

    vbf_idx_1 = ak.firsts(i1[mjj_idx])
    vbf_idx_2 = ak.firsts(i2[mjj_idx])

    # Calculate the variables for VBF event selection:
    #    mjj > 1 TeV
    #    dEtajj > 3
    #    pT(6-jets) < 65 GeV

    mjj = ak.firsts(mjj_cand[mjj_idx])
    dEtajj = abs(vbf_jet_1.eta - vbf_jet_2.eta)

    # The last cut (pT of the 6-jet system) needs us to select the HC jets
    sort_var, idx = getRRSort(evts, jarr)

    # Remove the central jets from the sort
    jidx = ak.Array([range(nj) for nj in evts.njets])
    is_vbf_cand = (jidx==vbf_idx_1) | (jidx==vbf_idx_2)

    sort_no_vbf = ak.where(is_vbf_cand, 0, sort_var)
    idx_no_vbf = ak.argsort(sort_no_vbf,ascending=False)

    vbf_pTvecsum = (v4[idx_no_vbf[:,:4]].sum() + vbf_jet_1 + vbf_jet_2).pt

    # Ensure that the vbf jets are not in the leading 4 jets
    sorted_var = ak.sort(sort_no_vbf,ascending=False)
    incl_vbf_in_hc = ak.prod(sorted_var[:,:4],axis=1)

    print(ak.sum(incl_vbf_in_hc==0),'evts where the HC jets are the VBF jets')
    vbf_pTvecsum = ak.where(incl_vbf_in_hc==0, np.inf, vbf_pTvecsum)

    """
    Save the cols for the vars defining the VBF cuts
    """
    evts['vbf_mjj'] = ak.fill_none(mjj,0)
    evts['vbf_dEtajj'] = ak.fill_none(dEtajj,-1)
    evts['vbf_pTvecsum'] = ak.fill_none(vbf_pTvecsum,0)

    pass_vbf_sel = (mjj > 1000) & (dEtajj > 3) & (vbf_pTvecsum < 65)
    pass_vbf_sel = ak.fill_none(pass_vbf_sel, False)

    # Then finally, retrun the indices of the HCs *not* in the sort
    idx_cen  = ak.where(pass_vbf_sel, idx_no_vbf[sort_no_vbf[idx_no_vbf]!=0], idx)

    return pass_vbf_sel, idx_cen


