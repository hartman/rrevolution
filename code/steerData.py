'''
Steer the data processing script for the GN2 b-taggers study

WARNING: The 2017 data _hasn't finished yet_ so will need to loop
back in on all of these checks.

Nicole Hartman
22 Feb 2024

'''

import os

for yr in [16,17,18,22]:

    cmd = f'python processMNTs.py --physicsSample data{yr} --prodTag FEB24 --ntag 4 --tagger DL1d --batch'
    os.system(cmd)
