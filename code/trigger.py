'''
Functions from Lucas for applying the trigger buckets.

Taken from this notebook:
https://gitlab.cern.ch/lborgna/hh4b-buckets/-/blob/master/Notebooks/Assigning%20Buckets%20with%20PAG%20NNT.ipynb

Modified for easyjet Ntuples
Mar 2023
'''

import numpy as np
import pandas as pd
import os
import awkward as ak
import operator as SmoothOperator

from utils import triggers_NR as triggers

def get_nr_bucket(t, evts, mask, year,jalias):
    '''
    Get the triggers for the year and the NR bucket.
    '''
    

    tDict = trigger_lut(year)

    if year >= 2022:
        if year == 2023:
            print('WARNING: need to sort out what to do with the j75 trigger')
        evts['bucket'] = evts[tDict['j80']].values.astype(int)
        print('In Run 3, only one trigger, no need to bucket')
        return

    jpts = t.arrays('pt',aliases=jalias).pt[mask]
    jpts_sort = ak.sort( jpts, ascending=False)

    evts['lead_pt']  = jpts_sort[:, 0].to_numpy()
    evts['third_pt'] = jpts_sort[:, 2].to_numpy()

    # num_jets = ak.count(jpts_sort,axis=1)
    # evts['third_pt'] = -1
    # mask = (num_jets > 3).to_numpy()
    # evts.loc[mask,'third_pt'] = jpts_sort[mask, 2].to_numpy()

    # Trigger matching -- still needs to be implemented in easyjet fw
    # for ti in triggers[year]:
    #     trigHashes = t['matchedTriggerHashes'].array()
    #     evts[ti] = ak.any(trigHashes==hashMap[ti],axis=1)

    # trig_cols = [f'triggerPassed_{ti}' for ti in triggers[year]]
    trig_cols = triggers[year]

    evts['trigger'] = evts[trig_cols].sum(axis=1).astype(bool)
    b1_mask = (evts['lead_pt'] > 170) & (evts['third_pt'] > 70)

    evts['bucket'] = 0
    evts.loc[ b1_mask & evts[tDict['2b1j']].values,'bucket'] = 1
    evts.loc[~b1_mask & evts[tDict['2b2j']].values,'bucket'] = 2

def trigger_lut(year: int) -> dict:
    """
    Lookup table for trigger keys for each year
    """
    if (year == 2016) or (year == 16):
        return {
            "1b": "HLT_j225_bmv2c2060_split",
            "2b1j": "HLT_j100_2j55_bmv2c2060_split",
            "2bHT": "HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25", 
            "2b2j": "HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25",
        }
    elif (year == 2017) or (year == 17):
        return {
            "1b": "HLT_j225_gsc300_bmv2c1070_split",
            "2b1j": "HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30",
            "2bHT": "HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21",
            "2b2j": "HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15p0ETA25",
        }
    elif (year == 2018) or (year == 18):
        return {
            "1b": "HLT_j225_gsc300_bmv2c1070_split",
            "2b1j": "HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30",
            "2bHT": "HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21",
            "2b2j": "HLT_2j35_bmv2c1060_split_2j35_L14J15p0ETA25",
        }

    elif (year == 2022) or (year == 22):
        return {
            "j80": "HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bdl1d77_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25",
        }

    elif (year == 2023) or (year == 23):
        return {
            "j75": "HLT_j75c_020jvt_j50c_020jvt_j25c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bgn177_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25",
            "j80": "HLT_j80c_020jvt_j55c_020jvt_j28c_020jvt_j20c_020jvt_SHARED_2j20c_020jvt_bgn177_pf_ftf_presel2c20XX2c20b85_L1J45p0ETA21_3J15p0ETA25",
        }
    
    else:
        print('')
        raise NotImplementedError

def getOnlineTag(df):
    '''
    Goal: Retrieve the online b-tagging decision for the jets that *failed* the
    offline 77% WP.

    To study whether we can use PC information, we wanted to investigate the
    impact of j dropping the events where the "not b" jet matched to an online b-tag.

    For each of the triggers matched and considered by XhhCommon, this function adds
    a new column: hlt_{1b,2b1j,2bHT,2b2j}_notOfflineTagOnlineDecision which is
    * Always False for 4b
    * Online decision for the "not b @ 77% offline WP" for 3b events
    * "Or" of online decsion for the 2b events

    Then we can veto these events by applying a ~ df[hlt_{1b,2b1j,2bHT,2b2j}_notOfflineTagOnlineDecision].
    '''

    trig_keys = ['1b','2b1j','2bHT','2b2j']

    # Loop over and consider each of the triggers separately
    for ti in trig_keys:
        # Init the columns
        df[f'hlt_{ti}_notOfflineTagOnlineDecision'] = False

        for ntag in [3,2]:

            mask = (df.ntag == ntag)

            notOfflineTag = ~df.loc[mask,[f'j{i}_btag' for i in range(4)]].values.astype(bool)
            onlineTag = df.loc[mask,[f'j{i}_sf_{ti}' for i in range(4)]].values[notOfflineTag].reshape(-1,2)

            df.loc[mask,f'hlt_{ti}_notOfflineTagOnlineDecision'] = np.any(onlineTag==1,axis=1)
