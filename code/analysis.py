'''
analyais.py

Goal: Given one of the multivariate (or baseline) pairing algs, this code just does evaluation
of looping over the jets, forming the pairs, and defining columns for the other analysis cut
(deta_hh and Xwt) and the kinematic regions

Updated for R22 easyjet Ntuples for HH4b NR
FEB 2024
'''

import numpy as np
import pandas as pd
import json
import matplotlib.pyplot as plt
from itertools import combinations, product
import os

import uproot
import awkward as ak
import coffea
from coffea.nanoevents.methods import vector
ak.behavior.update(vector.behavior)

from trigger import get_nr_bucket, trigger_lut 
from truth import truthMatchJets, getCorrectPair, getTruthJets
from preprocessUtils import transformData

import pyarrow as pa
import pyarrow.parquet as pq


vecDict = {'pt':'pt', 'eta':'eta','phi':'phi', 'mass':'m'}
mH = 125 # GeV


def get_vbf_sel(t, evts,jarr, four_jets, sort_var, idx, jcols, jalias):
    '''
    Get the vbf event selection.

    Inputs:
    - t: TTree (read by uproot)
    - evts: df w/ event level variables
    - jarr: Awkard array with the jet level features
    - four_jets: An event level cut (evt cleaning, 4 jets, etc.) 
                 to apply to new vars we're extracting from the tree
    - sort_var: The variable that we're sorting the jets by
        *  1: passes b-tagging
        *  0: fails b-tagging
        * -1: NOT valid (is a VBF jet)
        (Note, it was some more complicated logic for the RR code which 
        accomodated 3b1l and 3b1f categories, but simpler here b/c we don't
        have quantile yet for easyjet Ntuples.)
    - idx: The idx how we're sorting the analysis jets w/o vetoing the VBF jets
    - jcols / jalias: The columns and alias to load in for loading in the forward 
                      jets from the tree
    
    Outputs:
    - pass_vbf_sel
    - idx_cen: The indices where the first 4 correspond to the HC jets
    '''

    # Get the VBF jets
    farr = t.arrays( jcols, aliases=jalias, cut=f'(pt > 30) & (abs(eta) > 2.5) & (abs(eta) < 4.5)' )
    print('farr',len(farr))
    farr = farr[four_jets]

    print('jarr',len(jarr))
    print('farr',len(farr))

    j_all = ak.concatenate([jarr, farr], axis=1)

    # Form 4-vectors s.t. we can take the Cartesian product
    v4 = ak.zip( { k: j_all[v] for k,v in vecDict.items() }, with_name="PtEtaPhiMLorentzVector" )
    
    # For the event to be VBF, need at least 6 jets, 2 of which are anti-tagged
    evts['njets_all'] = ak.num(j_all.pt).to_numpy()
    num_untag = ak.sum(j_all.tag==0,axis=1)

    mask = (evts['njets_all'] >=6) & (num_untag>=2)

    jet_pairs = ak.combinations(   ak.mask(v4,mask),2)
    idx_pairs = ak.argcombinations(ak.mask(v4,mask),2)

    j1, j2 = ak.unzip(jet_pairs)
    i1, i2 = ak.unzip(idx_pairs)

    untagged_mask = (ak.mask(j_all['tag'],mask)[i1]==0) & (ak.mask(j_all['tag'],mask)[i2]==0)

    mjj_cand = ak.where(untagged_mask, (j1+j2).mass, 0)

    mjj = ak.argsort(mjj_cand,ascending=False)[:,:1]
    mjj_idx = ak.argsort(mjj_cand,ascending=False)[:,:1]

    # To *mask* when getting the HC jets (if the event passes the HC jet selection)
    vbf_jet_1 = ak.firsts(j1[mjj_idx])
    vbf_jet_2 = ak.firsts(j2[mjj_idx])

    vbf_idx_1 = ak.firsts(i1[mjj_idx])
    vbf_idx_2 = ak.firsts(i2[mjj_idx])

    # Calculate the variables for VBF event selection:
    #    mjj > 1 TeV
    #    dEtajj > 3
    #    pT(6-jets) < 65 GeV
    mjj = ak.firsts(mjj_cand[mjj_idx])
    dEtajj = abs(vbf_jet_1.eta - vbf_jet_2.eta)

    # The last cut (pT of the 6-jet system) needs us to select the HC jets
    # sort_var, idx = getRRSort(evts, jarr)

    # Remove the central jets from the sort
    jidx = ak.Array([range(nj) for nj in evts.njets])
    is_vbf_cand = (jidx==vbf_idx_1) | (jidx==vbf_idx_2)

    sort_no_vbf = ak.where(is_vbf_cand, -1, sort_var)
    idx_no_vbf = ak.argsort(sort_no_vbf,ascending=False)

    vbf_pTvecsum = (v4[idx_no_vbf[:,:4]].sum() + vbf_jet_1 + vbf_jet_2).pt

    # Ensure that the vbf jets are not in the leading 4 jets
    sorted_var = ak.sort(sort_no_vbf,ascending=False)
    incl_vbf_in_hc = ak.prod(sorted_var[:,:4],axis=1)

    print(ak.sum(incl_vbf_in_hc==0),'evts where the HC jets are the VBF jets')
    vbf_pTvecsum = ak.where(incl_vbf_in_hc==0, np.inf, vbf_pTvecsum)

    """
    Save the cols for the vars defining the VBF cuts
    """
    evts['vbf_mjj'] = ak.fill_none(mjj,0)
    evts['vbf_dEtajj'] = ak.fill_none(dEtajj,-1)
    evts['vbf_pTvecsum'] = ak.fill_none(vbf_pTvecsum,0)

    pass_vbf_sel = (mjj > 1000) & (dEtajj > 3) & (vbf_pTvecsum < 65)
    pass_vbf_sel = ak.fill_none(pass_vbf_sel, False)

    # Then finally, retrun the indices of the HCs *not* in the sort
    idx_cen  = ak.where(pass_vbf_sel, idx_no_vbf[sort_no_vbf[idx_no_vbf]!=0], idx)

    return pass_vbf_sel, idx_cen

def processDf(fname,tname="AnalysisMiniTree",nJetsMax=4,doVBF=True,
              pT_min=40,eta_max=2.5,
              tagger='DL1d',ftag_version='01',WP=77,sort='tag', min_btags=3,
              mc=True,truth=True, bjetTrigSFs=False, truthJets=False):
    '''
    Given an input MNT, get the relevant jet features and apply the ml preprocessing

    Inputs:
    - filename: Name of the mnt file to read in the name for
    - treename: The MNT tree name to read in
    - nJetsMax: Max # of jets to consider
    - doVBF: Whether not to categorize evts into a vbf and ggf SR based on the resolved ana
    - pT_min: Min jet pT to consider
    - eta_max: eta cut
    - sort: The sort to apply to the jets before truncating.
            Options: tag (default), pt (default), etc
    - min_btags: How many b-tags to save
    - mc: Flag for whether filename is mc
    - truth: flag for if truth info is stored in the MNT
     
    Returns:
    - df: Pandas df w/ the relevant jet and event level info calculated from the MNT.
    '''

    # Open the file
    t = uproot.open(f'{fname}:{tname}')

    # Infer the year from the MNT
    ycol = 'dataTakingYear'
    year = t.arrays([ycol],entry_stop=1,library='np')[ycol][0]

    # Get the triggers corresponding to this year
    trig_yr = trigger_lut(year)
    trig_cols = trig_yr.values()

    # Load in the corresponding arrays
    ecols = ['eventNumber','runNumber']
    ealias = {'pileupWeight':'PileupWeight_NOSYS'}
    if mc:
        ecols += ['pileupWeight'] 

    ecols += trig_cols
    for trig in trig_cols:
        ealias[trig] = f'trigPassed_{trig}'

    evts = t.arrays(ecols,aliases=ealias) 
    evts = pd.DataFrame(evts.to_numpy())

    print(f'len(t) = {len(evts)}')

    # Load in the mc event weight
    if mc:
        arr = t.arrays('mcEventWeights')
        evts['mcEventWeight'] = arr['mcEventWeights'][:,0]
        del arr

    print('Loading in the jet array')

    jetCol = 'antikt4PFlow'
    jcols = ['pt','eta','phi','m','tag','NNJvtPass']
    jcols_full = [f'recojet_{jetCol}_NOSYS_{v}' for v in 
                  ['pt','eta','phi','m', 
                   f'ftag_select_{tagger}v{ftag_version}_FixedCutBEff_{WP}','NNJvtPass']]
    if mc:
        jcols += ['flav']
        jcols_full += [f'recojet_{jetCol}_NOSYS_HadronConeExclTruthLabelID']

    jalias = {k: f'{v} / 1000' if (k=='pt' or k=='m') else v  
                for k,v in zip(jcols,jcols_full)}
    # jalias['sf'] = f'recojet_antikt4_NOSYS_SF_{tagger}_FixedCutBEff_{WP}'

    jarr = t.arrays(jcols, aliases=jalias, cut=f'(pt > {pT_min}) & (abs(eta) < {eta_max}) & NNJvtPass')

    # Mask events passing the trigger and 4-jet selections
    evts['njets'] = ak.num(jarr['pt']).to_numpy()
    evts['ntag'] = ak.sum(jarr['tag'],axis=1).to_numpy()

    four_jets = (evts['njets'] >=4) & (evts['ntag'] >= min_btags)
    print(f'Cutting on 4 jets and {min_btags} b-tags: ', four_jets.sum(),'evts')
    jarr = jarr[four_jets]
    evts = evts[four_jets.to_numpy()].reset_index(drop=True)
    if four_jets.sum() == 0:
        return None,None
    
    # Get the analysis trigger decision
    print(f'Applying {year} triggers')
    get_nr_bucket( t,evts, four_jets, year, jalias)
    
    print('Using the NR buckets')
    tmask = (evts.bucket > 0)
    jarr = jarr[tmask]
    evts = evts[tmask].reset_index(drop=True)

    '''
    Apply the jet selections
    '''
    # First sort by the jet pt
    idx0 = ak.argsort(jarr['pt'],ascending=False)
    jarr = jarr[idx0]

    # Now sort by whatever else (default btag decision)
    sort_var = jarr[sort]
    idx = ak.argsort(sort_var,ascending=False)

    print('idx',idx)
    # Finally, select the VBF jets
    if doVBF:
        print('Getting the VBF selection')
        m = np.zeros(t.num_entries,dtype='bool')
        m[four_jets] += tmask # a little clunky, but m[four_jets][tmask] = True wasn't supporting assignment in palce
        pass_vbf_sel, idx = get_vbf_sel(t, evts, jarr, m, sort_var, idx, jcols, jalias)
        evts['pass_vbf_sel'] = pass_vbf_sel.to_numpy()

    print('idx',idx)

    if len(idx) == 0:
        return None, None

    '''
    Apply the jet selections
    '''
    # Use the central njets (w/o the vbf included) to make X_wt
    print('Calculating X_wt')
    j4 = ak.zip( { k: jarr[v] for k,v in vecDict.items() }, with_name="PtEtaPhiMLorentzVector")
    print('len(jarr)',len(jarr))
    print('p4',j4)
    evts['X_wt_tag'] = getXwt(jarr, j4, idx)

    hc_jets = j4[idx[:,:nJetsMax]]
    
    if mc:

        # The ftag sfs use the same jet cuts as the central jets
        print('Calculating the SF')
        # jsfs = t.arrays('sf', aliases=jalias, cut=f'(pt > {pT_min}) & (abs(eta) < {eta_max}) & {jvtCut}')
        # mc_sf = ak.prod(jsfs.sf[:,:,0],axis=-1).to_numpy()
        mc_sf = np.prod(evts[['mcEventWeight','pileupWeight']],axis=1)

        evts['mc_sf'] = mc_sf

        # Also save the truth info
        if truth:

            print('Retrieving the truth information')

            # Get the b-quark array and 4-vectors
            tkeys = [k for k in t.keys() if 'truth_' in k]
            taliases = { k[6:] : f'{k}/1000' if ((k[-2:] == 'pt') or (k[-1] == 'm')) else k for k in tkeys}
            tarr = t.arrays(taliases.keys(),aliases=taliases)

            tarr = tarr[four_jets][tmask]

            H_bs = ak.zip( { k: tarr[f'bb_fromH1_{v}'] for k,v in vecDict.items() }, 
                        with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior )
            S_bs = ak.zip( { k: tarr[f'bb_fromH2_{v}'] for k,v in vecDict.items() }, 
                        with_name="PtEtaPhiMLorentzVector", behavior=vector.behavior )

            b4 = ak.concatenate([H_bs,S_bs],1)
        
            truthMatchJets(evts, hc_jets, b4, nJetsMax)
            getCorrectPair(evts, nJetsMax)

        if truthJets:
            raise NotImplementedError
            #getTruthJets(tree, evts, nJetsMax)

    return evts, hc_jets

def getXhh(m1, m2, c1=125, c2=125, res1=0.1, res2=0.1):
    '''
    Return the Xhh value for the event

    Inputs:
    - m1, m2: The 4-vectors for the leading and subleading HCs in the event
    - c1, c2: The center for the ellipse in the (lead,subl) HC mass plane
    - mres: The radius of the elipse, which will be multiplied by the HC mass

    Output:
    - Xhh: The Xhh value for the event, eq (3) from the 36 ifb int note
    '''
    return np.sqrt(((m1 - c1) / (res1 * m1))**2 + ((m2 - c2) / (res2 * m2))**2)

def getXwt(jarr, j4, idx):
    '''
    Calculate the X_wt variable for the event.

    Note: consistent w/ RR, only considers the HC jets (the first 4 jets in idx) for the
    b-tagged b-jets in the top-candidate.

    Input:
    - jarr: awkward array of jet features
    - ps: awkward array of 4-vectors for the jets
    - idx: ordering for the jets (first 4 jets are the HC jets in the eveny)

    Output:
    - Xwt: The Xwt minimized over all of the valid 3-jet combinations
    '''

    ps   = j4[idx]
    btag = jarr.tag[idx]

    bjet = ps[:,:4][btag[:,:4]==1]
    bidx = ak.Array([range(nb) for nb in ak.num(bjet)])

    # # add a dim across the last entry
    bjet = bjet[:,:,np.newaxis]
    bidx = bidx[:,:,np.newaxis]

    w_jet_pairs = ak.combinations(   ps,2)
    w_idx_pairs = ak.argcombinations(ps,2)

    wjet1, wjet2 = ak.unzip(w_jet_pairs[:,np.newaxis,:])
    widx1, widx2 = ak.unzip(w_idx_pairs[:,np.newaxis,:])

    # Get the corresponding combinations
    m_W = 80.4
    m_t = 172.5

    WC = (wjet1 + wjet2)
    tC = bjet + WC

    Xwt_combs = getXhh(tC.mass, WC.mass, m_t, m_W)

    # Set as "invalid" the entries where the b-jet overlaps w/ one of the w-jets
    Xwt_mask = ak.where((bidx == widx1) | (bidx==widx2),np.inf, Xwt_combs)

    # Minimize over the possible combinations + return
    return ak.min(ak.min(Xwt_mask,axis=-1),axis=-1).to_numpy()

'''
Some pairing functions
'''
def dr_lead(js, pair):
    '''
    Return the delta R of the leading HC
    '''
    pair_to_idx = {0: ((0,1), (2,3)),
                   1: ((0,2), (1,3)),
                   2: ((0,3), (1,2))}

    (ia0, ia1), (ib0, ib1) = pair_to_idx[pair]

    # pt ordering
    hc_sort = (js[:,ia0]+js[:,ia1]).pt > (js[:,ib0]+js[:,ib1]).pt

    dr = np.where(hc_sort, js[:,ia0].delta_r(js[:,ia1]), js[:,ib0].delta_r(js[:,ib1]))

    return dr.to_numpy()

def do_min_dR(evts,hc_jets):
    '''
    Goal: Do the baseline analysis selection.

    Inputs:
    - evts
    - hc_jets
    '''

    evts['chosenPair'] = np.argmin( np.vstack([dr_lead(hc_jets, i) for i in range(3)]), axis=0)    

    ja0 = hc_jets[:,0]
    print('chosen pair',np.unique(evts['chosenPair'].values.astype(int)))
    
    ja1 = hc_jets[range(len(hc_jets)),evts['chosenPair'].values.astype(int)+1]

    jb0 = ak.where(evts['chosenPair']==0, hc_jets[:,2], hc_jets[:,1])
    jb1 = ak.where(evts['chosenPair']==2, hc_jets[:,2], hc_jets[:,3])

    hca = ja0+ja1
    hcb = jb0+jb1
    
    # The scalar candidate with the mass closest to the Higgs mass 
    # will be the Higgs candidate
    sort_mask = hca.pt > hcb.pt
    hc1 = ak.where(sort_mask, hca, hcb) # leading pt HC
    hc2 = ak.where(sort_mask, hcb, hca)

    return hc1, hc2


def defineCuts(evts, hc1, hc2, c1=124, c2=117,res1=.1,res2=.1):
    '''
    Goal: Save some of the useful variables for defining the final SR:
    - dEta_HH
    - m_{S,H}
    - correct
    '''
   
    hh = hc1 + hc2
    evts['m_hh'] = hh.mass.to_numpy()
    evts['pt_hh'] = hh.pt.to_numpy()

    for i,hc in zip([1,2],[hc1,hc2]):

        evts[f'pt_h{i}']  = hc.pt.to_numpy()
        evts[f'eta_h{i}'] = hc.eta.to_numpy()
        evts[f'phi_h{i}'] = hc.phi.to_numpy()
        evts[f'm_h{i}']   = hc.mass.to_numpy()

    evts['dEta_hh'] = abs(hc1.eta - hc2.eta).to_numpy()
    evts['X_hh'] = getXhh(hc1.mass,hc2.mass,c1,c2,res1,res2).to_numpy() 

    # Make a mask that's easier to check:
    if 'correctPair' in evts.columns:
        evts['correct'] = evts['chosenPair'] == evts['correctPair']

def pairAndProcess(inputFile: str, ntag=3, tagger='DL1d', ftag_version='01', WP=77, pairing='min_dR',
                   cols: list = [], physicsSample=None, nSelectedJets=4,pT_min=40,
                   save=False,fileTag='',truth=False,
                   outputDir='',
                   bjetTrigSFs=False,truthJets=False):
    '''
    Given a MNT, select the desired jets, do the GNN preprocessing, and apply
    the analysis cuts and return a df with a given # of btags in a specified
    kinematic region with only a subset of the columns.

    Inputs:
    - inputFile: The MNT file to evalutate on
    '''

    '''
    Step 1: Apply the jet selection
    '''
    is_mc = False if ('data' in physicsSample) else True
    df, hc_jets = processDf(inputFile, min_btags=ntag, tagger=tagger, ftag_version=ftag_version,WP=WP,
                            nJetsMax=nSelectedJets, pT_min=pT_min, 
                            mc=is_mc, truth=truth,truthJets=truthJets)

    outputFile = f'{outputDir}/files/df{fileTag}.parquet'

    # Sometimes, we might not have any entries left
    if df is None: 
        print('No entries in file',inputFile)
        
        # Save an empty df
        table  = pa.Table.from_pandas(pd.DataFrame())
        pq.write_table(table,outputFile)
        return None

    '''
    Step 2: pair the HCs
    '''
    assert nSelectedJets == 4

    if (pairing == 'min_dR'):  
        hc1, hc2 = do_min_dR(df,hc_jets)
    else:
        print('pairing',pairing,'not supported')
        raise NotImplementedError
    
    '''
    Step 3: Apply the analysis cuts
    '''        
    defineCuts(df, hc1, hc2, c1=124, c2=117)

    for i in range(nSelectedJets):
        df[f'j{i}_pt'] = hc_jets[:,i].pt
        df[f'j{i}_eta'] = hc_jets[:,i].eta
        df[f'j{i}_phi'] = hc_jets[:,i].phi
        df[f'j{i}_E'] = hc_jets[:,i].energy

    '''
    Step 4: Save and return the df
    '''
    if save:
        print('Save the parquet file',outputFile)
        table  = pa.Table.from_pandas(df)
        pq.write_table(table,outputFile)

    return df
