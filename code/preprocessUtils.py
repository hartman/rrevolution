'''
Grouping together some of the preprocessing functions that I've been using.
'''
import numpy as np
import json 

def scale(X, mask, featureIndices=[0,3],var_names=None, filename='test.json',savevars=True):
    '''
    Inputs:
    - X: Data in the form of (events, jets, features)
    - mask: Indicates the unmasked values of X
    - featureIndices: the indices indicating which vars to take the log of
    - var_names:
    - fileame: Name of the scaling file to write the output to
    - savevars:

    Modifies the data in place

    Reference: Taken from Micky's dataprocessing.py file in
    https://github.com/mickypaganini/RNNIP
    '''

    scale = {}

    if var_names is None:
        var_names = [f'v{i}' for i in featureIndices]

    if savevars:

        for vi,v in zip(featureIndices,var_names):
            print(f'Scaling {v}.')
            f = X[:, :, vi]
            slc = f[mask]
            m, s = slc.mean(), slc.std()
            slc -= m
            slc /= s
            X[:, :, vi][mask] = slc.astype('float32')
            scale[v] = {'mean' : float(m), 'sd' : float(s)}

        with open(filename, 'w') as varfile:
            json.dump(scale, varfile)

    else:
        with open(filename, 'r') as varfile:
            varinfo = json.load(varfile)

        for vi,v in zip(featureIndices,var_names):
            print(f'Scaling {v}.')
            f = X[:, :, vi]
            slc = f[mask]
            m = varinfo[v]['mean']
            s = varinfo[v]['sd']
            slc -= m
            slc /= s
            X[:, :, vi][mask] = slc.astype('float32')
            
            
def transformData(df, jVars, scalingFile='',
                  nSelectedJets=5, log_cutoff=40):
    '''
    Apply the scaling for the jet variables, and return the df.
    '''

    print(jVars)
    X_all = np.dstack([df[[f'j{i}_{v}' for i in range(nSelectedJets)]].values for v in jVars])
    m_all =  ~ np.all(X_all==0, axis=-1)

    # pT and E normalization (jvc too - if need be?)
    log_idx = [i for i,v in enumerate(jVars) if v=='pt' or v=='E']
    for vi in log_idx:
        X_all[:,:,vi][m_all] = np.log(X_all[:,:,vi][m_all]-log_cutoff)

    featureIndices = log_idx
    if 'jvc' in jVars: featureIndices.append('jvc')
    jet_vars = [jVars[i] for i in featureIndices]

    scale(X_all,m_all,featureIndices,jet_vars,filename=scalingFile,savevars=False)

    # Center Db
    if 'Db' in jVars:
        # Need to retrieve the index Db (I think)
        i_Db = jVars.index('Db')
        X_all[:,:,i_Db][m_all] -= 3 # shift Db bins

    if 'btag' in jVars:
        i_btag = jVars.index('btag')
        X_all[:,:,i_btag][m_all] -= 0.5 # shift b-tag decisions 

    # save the output columns
    for vi,v in enumerate(jVars):
        for i in range(nSelectedJets):
            df[f'ml_j{i}_{v}'] = X_all[:,i,vi]

    return df
