/*
 * Code which prints out the hashes corresponing to each of the triggers.
 * To save the output, you can run the macros and save the output 
 *
 * root -l -q getHashes.C > hashMap.json
 *
 * But right now, you need to edit this macros to put the .json
 * file in the proper format as well.
 *
 * May 2019
 * */

#include <vector>
using namespace std;

void getHashes()
{

    vector<string> trigger_list = {
		// 2015
        "HLT_2j35_btight_2j35_L14J15.0ETA25",
        "HLT_2j35_btight_2j35_L13J25.0ETA23",
        "HLT_j100_2j55_bmedium",
        "HLT_j225_bloose",
		// 2016
        "HLT_2j35_bmv2c2060_split_2j35_L14J15.0ETA25",
        "HLT_j100_2j55_bmv2c2060_split",
        "HLT_j225_bmv2c2060_split",
		// 2017
		"HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15.0ETA25",
		"HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21",
		"HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30",
		"HLT_j225_gsc300_bmv2c1070_split",
		"HLT_j35_gsc55_bmv2c1050_split_ht700_L1HT190-J15s5.ETA21",
		// 2018
		"HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190-J15s5.ETA21",
		"HLT_2j35_bmv2c1060_split_2j35_L14J15.0ETA25",
		"HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30"
	};

    for( string trigger : trigger_list ){
        cout << "\"" << trigger << "\" : " << hash<string>{}(trigger) << ",\n";
    }
    return;
}
