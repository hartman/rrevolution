'''
Goal: Collate the .parquet files and plot the massplanes using dask 
and save the corresponding cutflows as well.

Nicole Hartman
Nov 2020
'''

import dask
import dask.dataframe as dd
from dask.distributed import Client

import numpy as np
import pandas as pd
import h5py
import os
import time

import warnings
warnings.simplefilter("ignore")


def get_deta_cut(df):
    return df.assign(deta_cut=(df.eta_h1-df.eta_h2).abs() < 1.5)

def getCutflow(subDir, pairing, dataDir='../../data'):
    '''
    '''

    client = Client(n_workers=4)
    
    # Load in the .parquet files 
    in_dir = f'{dataDir}/{subDir}/files'
    print(f'{in_dir}/df_period?_*_{pairing}_2b.parquet')
    if os.path.exists(f'{in_dir}/df_period?_*_{pairing}_2b.parquet'):
        dat_2b = dd.read_parquet(f'{in_dir}/df_period?_*_{pairing}_2b.parquet')
    else:
        dat_2b = dd.read_hdf(f'{in_dir}/df_period?_*_{pairing}_2b.h5','df')
        
        
    if os.path.exists(f'{in_dir}/df_period?_*_{pairing}_3b.parquet'):
        dat_3b = dd.read_parquet(f'{in_dir}/df_period?_*_{pairing}_3b.parquet')
    else:
        dat_3b = dd.read_hdf(f'{in_dir}/df_period?_*_{pairing}_3b.h5','df')
        
    # Define the deta_hh cuts
    meta = {c:dat_2b[c].dtype for c in dat_2b.columns}
    meta['deta_cut']= 'bool'
    
    dat_2b = dat_2b.map_partitions(get_deta_cut,meta=meta)
    dat_3b = dat_3b.map_partitions(get_deta_cut,meta=meta)
    
    idx1 = ['4 jets','MDR','MDpT','deta_hh','X_wt','SR','VR','CR']
    idx2 = ['4 jets','MDR',       'deta_hh','X_wt','SR','VR','CR']
    cols = ['2b','3b','4b']
    df_MDpT   = pd.DataFrame(0, idx1, cols)
    df_noMDpT = pd.DataFrame(0, idx2, cols)
    
    # Calculate the 2b cutflow w/o MDpT
    a0 = dat_2b.reduction(lambda x: (x.ntag==2).sum(),
                              aggregate=lambda x: x.sum())
    a1 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR).sum(),
                          aggregate=lambda x: x.sum())
    a2 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR & x.MDpT).sum(),
                          aggregate=lambda x: x.sum())
    a3 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR & x.MDpT & x.deta_cut).sum(),
                          aggregate=lambda x: x.sum())
    a4 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR & x.MDpT & x.deta_cut & (x.X_wt > 1.5)).sum(),
                          aggregate=lambda x: x.sum())
    a5 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==0)).sum(),
                          aggregate=lambda x: x.sum())
    a6 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==1)).sum(),
                          aggregate=lambda x: x.sum())
    a7 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==2)).sum(),
                          aggregate=lambda x: x.sum())

    # And now... w/o MDpT
    d0 = dat_2b.reduction(lambda x: (x.ntag==2).sum(),
                          aggregate=lambda x: x.sum())
    d1 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.MDR).sum(),
                          aggregate=lambda x: x.sum())
    d3 = dat_2b.reduction(lambda x: ((x.ntag==2)& x.deta_cut).sum(),
                          aggregate=lambda x: x.sum())
    d4 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.deta_cut & (x.X_wt > 1.5)).sum(),
                          aggregate=lambda x: x.sum())
    d5 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==0)).sum(),
                          aggregate=lambda x: x.sum())
    d6 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==1)).sum(),
                          aggregate=lambda x: x.sum())
    d7 = dat_2b.reduction(lambda x: ((x.ntag==2) & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==2)).sum(),
                          aggregate=lambda x: x.sum())

    cuts_2b = dask.compute(a0,a1,a2,a3,a4,a5,a6,a7, d0,d1,d3,d4,d5,d6,d7)
    df_MDpT.loc[  idx1,'2b'] = cuts_2b[:8]
    df_noMDpT.loc[idx2,'2b'] = cuts_2b[8:]
    
    # And now - for 3b and 4b!
    
    b0 = dat_3b.reduction(lambda x: ((x.ntag==3) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    b1 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    b2 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    b3 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    b4 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & (x.X_wt > 1.5) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    # b5 - SR blinded
    b6 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==1)).sum(),
                          aggregate=lambda x: x.sum())
    b7 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==2)).sum(),
                          aggregate=lambda x: x.sum())
    
    c0 = dat_3b.reduction(lambda x: ((x.ntag>=4) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    c1 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    c2 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.MDpT & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    c3 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.MDpT & x.deta_cut & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    c4 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.MDpT & x.deta_cut & (x.X_wt > 1.5) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    # c5 - SR blinded
    c6 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==1)).sum(),
                          aggregate=lambda x: x.sum())
    c7 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==2)).sum(),
                          aggregate=lambda x: x.sum())
    
    # And now w/o the MDpT 
    e0 = dat_3b.reduction(lambda x: ((x.ntag==3) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    e1 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    # e2 - for MDpT
    e3 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    e4 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & (x.X_wt > 1.5) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    # b5 - SR blinded
    e6 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==1)).sum(),
                          aggregate=lambda x: x.sum())
    e7 = dat_3b.reduction(lambda x: ((x.ntag==3) & x.MDR & x.MDpT & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==2)).sum(),
                          aggregate=lambda x: x.sum())
    
    f0 = dat_3b.reduction(lambda x: ((x.ntag>=4) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    f1 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    # f2 - for MDpT
    f3 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.deta_cut & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    f4 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.deta_cut & (x.X_wt > 1.5) & (x.kinematic_region != 0)).sum(),
                          aggregate=lambda x: x.sum())
    # c5 - SR blinded
    f6 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==1)).sum(),
                          aggregate=lambda x: x.sum())
    f7 = dat_3b.reduction(lambda x: ((x.ntag>=4) & x.MDR & x.deta_cut & \
                          (x.X_wt > 1.5) & (x.kinematic_region==2)).sum(),
                          aggregate=lambda x: x.sum())
    
    
    cuts_3and4b = dask.compute(b0,b1,b2,b3,b4,b6,b7, e0,e1,e3,e4,e6,e7,
                               c0,c1,c2,c3,c4,c6,c7, f0,f1,f3,f4,f6,f7)
    cuts_3b = cuts_3and4b[:13]
    cuts_4b = cuts_3and4b[13:]

    df_MDpT.loc[  idx1[:5]+idx1[6:],'3b'] = cuts_3b[:7]
    df_noMDpT.loc[idx2[:4]+idx2[5:],'3b'] = cuts_3b[7:]

    df_MDpT.loc[  idx1[:5]+idx1[6:],'4b'] = cuts_4b[:7]
    df_noMDpT.loc[idx2[:4]+idx2[5:],'4b'] = cuts_4b[7:]
    
    print('Done caluclating cutflow - saving dfs')
    
    # Save the pandas dfs
    pout = pairing.replace('_*','') 
    output_pandas = f'{dataDir}/{subDir}/cutflow_{pout}_blinded.h5'
    df_MDpT.to_hdf(  output_pandas,key='df_MDpT'  )
    df_noMDpT.to_hdf(output_pandas,key='df_noMDpT')
    
    
    print('Loading dfs into memory for massplanes')
    # Save a json file w/ the massplanes
    df_2b = dat_2b.loc[dat_2b.deta_cut, ['m_h1','m_h2','MDpT','X_wt']].compute()
    df_3b = dat_3b.loc[(dat_3b.kinematic_region != 0) & dat_3b.deta_cut, 
                       ['m_h1','m_h2','MDpT','X_wt','ntag']].compute()

    mps = {}
    b = 25
    rs = [[50, 200],[50, 200]]

    '''
    # First w/o MDpT
    mp_deta_2b = dat_2b.reduction(lambda x: np.histogram2d(*x[['m_h1','m_h2']].values.T,b,rs)[0], 
                                  aggregate=lambda x: x.sum())
    mp_deta_3b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[x.ntag==3,['m_h1','m_h2']].values.T,b,rs)[0],
                                  aggregate=lambda x: x.sum())
    mp_deta_4b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[x.ntag>=4,['m_h1','m_h2']].values.T,b,rs)[0],
                                  aggregate=lambda x: x.sum())

    mp_deta_Xwt_2b = dat_2b.reduction(lambda x: np.histogram2d(*x.loc[x.X_wt > 1.5,['m_h1','m_h2']].values.T,b,rs)[0],
                                      aggregate=lambda x: x.sum())
    mp_deta_Xwt_3b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[(x.ntag==3) & (x.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0],
                                      aggregate=lambda x: x.sum())
    mp_deta_Xwt_4b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[(x.ntag>=4) & (x.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0],
                                      aggregate=lambda x: x.sum())

    # And now with
    mp_MDpT_deta_2b = dat_2b.reduction(lambda x: np.histogram2d(*x.loc[x.MDpT,['m_h1','m_h2']].values.T,b,rs)[0],
                                       aggregate=lambda x: x.sum())
    mp_MDpT_deta_3b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[(x.ntag==3) & x.MDpT,['m_h1','m_h2']].values.T,b,rs)[0],
                                       aggregate=lambda x: x.sum())
    mp_MDpT_deta_4b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[(x.ntag>=4) & x.MDpT,['m_h1','m_h2']].values.T,b,rs)[0],
                                       aggregate=lambda x: x.sum())

    mp_MDpT_deta_Xwt_2b = dat_2b.reduction(lambda x: np.histogram2d(*x.loc[x.MDpT & (x.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0],
                                           aggregate=lambda x: x.sum())
    mp_MDpT_deta_Xwt_3b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[(x.ntag==3) & x.MDpT & (x.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0],
                                           aggregate=lambda x: x.sum())
    mp_MDpT_deta_Xwt_4b = dat_3b.reduction(lambda x: np.histogram2d(*x.loc[(x.ntag>=4) & x.MDpT & (x.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0],
                                           aggregate=lambda x: x.sum())

    vals = dask.compute(mp_deta_2b,          mp_deta_3b,          mp_deta_4b, 
                        mp_deta_Xwt_2b,      mp_deta_Xwt_3b,      mp_deta_Xwt_4b,
                        mp_MDpT_deta_2b,     mp_MDpT_deta_3b,     mp_MDpT_deta_4b,
                        mp_MDpT_deta_Xwt_2b, mp_MDpT_deta_Xwt_3b, mp_MDpT_deta_Xwt_4b)
    
    mps['mp_deta_2b'],          mps['mp_deta_3b'],          mps['mp_deta_4b']          = vals[:3] 
    mps['mp_deta_Xwt_2b'],      mps['mp_deta_Xwt_3b'],      mps['mp_deta_Xwt_4b']      = vals[3:6] 
    mps['mp_MDpT_deta_2b'],     mps['mp_MDpT_deta_3b'],     mps['mp_MDpT_deta_4b']     = vals[6:9] 
    mps['mp_MDpT_deta_Xwt_2b'], mps['mp_MDpT_deta_Xwt_2b'], mps['mp_MDpT_deta_Xwt_2b'] = vals[9:] 
    '''

    # First w/o MDpT
    mps['mp_deta_2b'] = np.histogram2d(*df_2b[['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_deta_3b'] = np.histogram2d(*df_3b.loc[df_3b.ntag==3,['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_deta_4b'] = np.histogram2d(*df_3b.loc[df_3b.ntag>=4,['m_h1','m_h2']].values.T,b,rs)[0]

    mps['mp_deta_Xwt_2b'] = np.histogram2d(*df_2b.loc[df_2b.X_wt > 1.5,['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_deta_Xwt_3b'] = np.histogram2d(*df_3b.loc[(df_3b.ntag==3) & (df_3b.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_deta_Xwt_4b'] = np.histogram2d(*df_3b.loc[(df_3b.ntag>=4) & (df_3b.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0]

    # And now with
    mps['mp_MDpT_deta_2b'] = np.histogram2d(*df_2b.loc[df_2b.MDpT,['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_MDpT_deta_3b'] = np.histogram2d(*df_3b.loc[(df_3b.ntag==3) & df_3b.MDpT,['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_MDpT_deta_4b'] = np.histogram2d(*df_3b.loc[(df_3b.ntag>=4) & df_3b.MDpT,['m_h1','m_h2']].values.T,b,rs)[0]

    mps['mp_MDpT_deta_Xwt_2b'] = np.histogram2d(*df_2b.loc[df_2b.MDpT & (df_2b.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_MDpT_deta_Xwt_3b'] = np.histogram2d(*df_3b.loc[(df_3b.ntag==3) & df_3b.MDpT & (df_3b.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0]
    mps['mp_MDpT_deta_Xwt_4b'] = np.histogram2d(*df_3b.loc[(df_3b.ntag>=4) & df_3b.MDpT & (df_3b.X_wt>1.5),['m_h1','m_h2']].values.T,b,rs)[0]

    # Write the output to a json file
    output_h5 = f'{dataDir}/{subDir}/massplanes_{pout}_blinded.h5'
    h5f = h5py.File(output_h5, 'w')
    for k, v in mps.items():
        h5f.create_dataset(k,data=v)
    print(h5f.keys())

    # Close the dask client
    client.close()
    
    # Return the massplanes
    return mps

# If the eda tools is parallel to RRevolution, draw the massplane as well 
def drawAllMassplanes(mps):
    '''
    '''       
    pass
    
     

if __name__ == '__main__':

    from argparse import ArgumentParser
    p = ArgumentParser()

    # Start off just using the same OptionParser arguments that Zihao used.
    p.add_argument('--pairAGraphConfig','--pconfig',dest='pconfig',
                   type=str,default='SM_2b',
                   help='the config file to use for pairAGraph, default SM_2b')
    p.add_argument('--pairing',type=str,default='pag',
                   help="Pairing algo to use, one of pag (default), MDR, min_dR")

    p.add_argument('--physicsSample', type=str, default='data16',
                   help="The physics sample to run over (default data16)")
    p.add_argument('--prodTag', type=str, default='FEB20',
                   help="The production tag for the miniNtuple (default FEB20).")
    p.add_argument('--nSelectedJets',type=int,default=5,help="Number of jets for GNN")
    p.add_argument('--pTcut',type=int,default=40,help="pT cut to use for the jets (default 40 GeV)")

    args = p.parse_args()

    # Get the subDir 
    subDir = f'{args.physicsSample}_PFlow-{args.prodTag}'
    if args.nSelectedJets != 4:
        subDir += f'-{args.nSelectedJets}jets'
    if args.pTcut != 40:
        subDir += f'-{args.pTcut}'
    
    # Get the (wildcard) for the pairing algo
    pairing = f'{args.pconfig}_*' if args.pairing == 'pag' else args.pairing

    # J for funsies - time the function
    start = time.time()

    # Calculate the cutflows 
    mp_dict = getCutflow(subDir, pairing)
    
    stop = time.time()
    
    run = stop - start
    print(f'Run time: {run // 60} min {run % 60} s')
    
    




