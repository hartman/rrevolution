'''
Functions from the partial Run 2 analysis
'''

def lead_dRjj_cut(dRjj, m4j):
    '''
    Given the opening angle of the constituents of the leading HC
    (ordered by the SS of the constituents), return true or false
    for whether or not the opening angle is within the envelope
    given by the 4 jet invariant mass.

    Inputs:
    - dRjj: A np array of shape (nEvts,)
    - m4j: A np array of shape (nEvs,)
    '''

    mask = np.ones_like(dRjj).astype(bool)

    # low mass cut
    mask[(m4j < 1250) & (dRjj < 360 / m4j - 0.5)] = False
    mask[(m4j < 1250) & (dRjj > 652.863 / m4j + 0.474449)] = False

    # high mass cut
    mask[(m4j > 1250) & (dRjj < 0)] = False
    mask[(m4j > 1250) & (dRjj > 0.9967394)] = False

    return mask

def subl_dRjj_cut(dRjj, m4j):
    '''
    dR cut for the subleading HC
    '''

    mask = np.ones_like(dRjj).astype(bool)

    # low mass cut
    mask[(m4j < 1250) & (dRjj < 235.242 / m4j + 0.0162996)] = False
    mask[(m4j < 1250) & (dRjj > 874.890 / m4j + 0.347137)] = False

    # high mass cut
    mask[(m4j > 1250) & (dRjj < 0)] = False
    mask[(m4j > 1250) & (dRjj > 1.047049)] = False

    return mask

def MDR_cut(lead_dR_arr, subl_dR_arr, m4j):
    '''
    Determine which of the pairings satisfy the MDR cut

    Inputs:
    - lead_dRarr: Array of the opening angle between the jets of the leading HC
    - subl_dRarr: Array of the opening angle between the jets of the subleading HC
    - m4j: The invariant mass of the 4-jets

    Output:
    - mask: A boolean np array for
    '''
    lead_mask = lead_dRjj_cut(lead_dR_arr, m4j)
    subl_mask = subl_dRjj_cut(subl_dR_arr, m4j)
    mask = lead_mask & subl_mask

    return mask


def MDR_minDhh(df):
    '''
    Plan: Loop of the # valid pairings options and save the HCs
    '''

    # MDR cuts
    dRjj_h1s = df[[f'dRjj_h1_pair{i}' for i in range(3)]].values
    dRjj_h2s = df[[f'dRjj_h2_pair{i}' for i in range(3)]].values
    valid_mask = MDR_cut(dRjj_h1s, dRjj_h2s, df.m_hh.values.reshape(-1,1))

    df['nValidPairs'] = np.sum(valid_mask,axis=1)
    df['MDR'] = (df.nValidPairs > 0)

    # If nValidPairs is 0, fill HCs w/ 0 (j take care of as initialization step)
    df['chosenPair'] = -1
    for hi,v in product([1,2],['pT','eta','phi','m','dRjj']):
        df[f'{v}_h{hi}'] = 0

    # If nValidPairs is 1, no min necessary
    m = df.nValidPairs==1
    for hi,v in product([1,2],['pT','eta','phi','m','dRjj','dPhi']):
        k = f'{v}_h{hi}'
        df.loc[m,k] = df.loc[m,[f'{k}_pair{i}' for i in range(3)]].values[valid_mask[m]]

    # Save the chosen pair
    vp_idx = np.vstack([i * np.ones(np.sum(m)) for i in range(3)]).T
    df.loc[m,'chosenPair'] = vp_idx[valid_mask[m]]

    # Otherwise minimize over Dhh - so first we need to *define* Dhh!!
    s = 120 / 110
    for i in range(3):
        df[f'Dhh_pair{i}'] = np.abs(df[f'm_h1_pair{i}'] - s * df[f'm_h2_pair{i}']) / np.sqrt(1+s**2)

    for vp in [2,3]:

        m = df.nValidPairs==vp
        N = np.sum(m)

        vp_idx = np.vstack([i * np.ones(N) for i in range(3)]).T
        i_min = np.argmin(df.loc[m,[f'Dhh_pair{i}' for i in range(3)]].values[valid_mask[m]].reshape(-1,vp),axis=1)

        df.loc[m,'chosenPair'] = vp_idx[valid_mask[m]].reshape(-1,vp)[np.arange(N),i_min]

        # And now save all the other (relevant) HC variables
        for hi,v in product([1,2],['pT','eta','phi','m','dRjj','dPhi']):
            k = f'{v}_h{hi}'
            vals = df.loc[m,[f'{k}_pair{i}' for i in range(3)]].values[valid_mask[m]].reshape(-1,vp)
            df.loc[m,k] = vals[np.arange(N),i_min]

        # Dbs
        for hi,ji in product([1,2],[1,2]):
            for v in ['pt','eta','phi','E','Db']:
                k = f'{v}_h{hi}_j{ji}'
                vals = df.loc[m,[f'{k}_pair{i}' for i in range(3)]].values[valid_mask[m]].reshape(-1,vp)
                df.loc[m,k] = vals[np.arange(N),i_min]

def MDpT_cut(df):
    '''
    Apply the pT sort and derive the corresponding MDpT mask
    '''

    df['lead_HC_pt'] = np.where(df.pT_h1>df.pT_h2, df.pT_h1, df.pT_h2)
    df['subl_HC_pt'] = np.where(df.pT_h1>df.pT_h2, df.pT_h2, df.pT_h1)

    lead_pT_cut = 0.513333 * df.m_hh - 103.3333
    subl_pT_cut = 0.33333  * df.m_hh -  73.3333

    df['MDpT'] = (df.lead_HC_pt > lead_pT_cut) & (df.subl_HC_pt > subl_pT_cut)
