'''
Implementation of the pairAGraph models under consideration
'''

import numpy as np
import pandas as pd
import copy 
import json
from itertools import combinations

import torch
import torch.nn as nn
import torch.nn.functional as F

import os
import sys
from argparse import ArgumentParser

#from preprocess import getNumPairs, prepareData
#from GNNPlots import trainingMetrics
from prepareData import getNumPairs, getDataLoaders


def _get_clones(module, N):
    return nn.ModuleList([copy.deepcopy(module) for i in range(N)])

    # Load in the model
    #model = pairAGraph(inpt_dim=5,embed_dim=embed_dim,ff_dim=ff_dim,
    #                   nAttnBlocks=nLayers,nHeads=nHeads,p=p,njets=nSelectedJets,scoreFct=scoreFct)

    #params = sum([np.prod(p.size()) for p in model.parameters()])
    #print(f"Training a transformer model with {nLayers} encoder layers with {nHeads} heads")
    #print(f"embed_dim = {embed_dim}, ff_dim = {ff_dim}, dpt = {p}, lr = {lr}.")
    #print(f"{params} trainable parameters")

    #train(model,loader_train,loader_val,lr,nEpochs,patience,device,modelDir,figDir,args.masked)

