'''
Implements a transformer for jet pairing
'''

import torch
from torch import nn
import torch.nn.functional as F
import copy
from typing import Optional
from itertools import combinations
import json 

def _get_clones(module, N):
    return ModuleList([copy.deepcopy(module) for i in range(N)])

def _get_activation_fn(activation):
    if activation == "relu":
        return F.relu
    elif activation == "gelu":
        return F.gelu

    raise RuntimeError("activation should be relu/gelu, not {}".format(activation))


class TransformerPreLNEncoderLayer(nn.Module):
    r"""
    Copied from the pytorch TransformerEncoderLayer source code, but modified
    by the suggestion in 2002.04745.
    """

    def __init__(self, d_model, nhead, dim_feedforward=2048, dropout=0.1, activation="relu"):
        super(TransformerPreLNEncoderLayer, self).__init__()
        self.self_attn = nn.MultiheadAttention(d_model, nhead, dropout=dropout)
        # Implementation of Feedforward model
        self.linear1 = nn.Linear(d_model, dim_feedforward)
        self.dropout = nn.Dropout(dropout)
        self.linear2 = nn.Linear(dim_feedforward, d_model)

        self.norm1 = nn.LayerNorm(d_model)
        self.norm2 = nn.LayerNorm(d_model)
        self.dropout1 = nn.Dropout(dropout)
        self.dropout2 = nn.Dropout(dropout)

        self.activation = _get_activation_fn(activation)

    def __setstate__(self, state):
        if 'activation' not in state:
            state['activation'] = F.relu
        super(TransformerPreLNEncoderLayer, self).__setstate__(state)

    def forward(self, src: torch.Tensor, src_mask: Optional[torch.Tensor] = None, src_key_padding_mask: Optional[torch.Tensor] = None) -> torch.Tensor:
        r"""Pass the input through the encoder layer.

        Args:
            src: the sequence to the encoder layer (required).
            src_mask: the mask for the src sequence (optional).
            src_key_padding_mask: the mask for the src keys per batch (optional).

        Shape:
            see the docs in Transformer class.
        """
        src = self.norm1(src)
        src2 = self.self_attn(src, src, src, attn_mask=src_mask,
                              key_padding_mask=src_key_padding_mask)[0]
        src = src + self.dropout1(src2)
        src = self.norm2(src)
        src2 = self.linear2(self.dropout(self.activation(self.linear1(src))))
        src = src + self.dropout2(src2)
        return src

class jetCompatibility(nn.Module):
    '''
    TorchScript friendly score function. The script function needed:
    1. No double list comprehensions
    2. itertools.combimnations -> torch.combinations
    But - it seems like this should be able to handle the arbitrary len inputs!! 
    '''
    def __init__(self,nSelectedJets=5):
        super(jetCompatibility, self).__init__()

        self.njets=nSelectedJets

    def forward(self,h,e: Optional[torch.Tensor]=None):

        combs = []

        for i in torch.combinations(torch.arange(self.njets),4): 
            for (h11,h12), (h21, h22) in zip([(i[0],i[1]),(i[0],i[2]), (i[0],i[3])],
                                             [(i[2],i[3]),(i[1],i[3]),(i[1],i[2])]):

                jetProj = (torch.bmm(h[h11][:,None], h[h12][:,:,None]) \
                           + torch.bmm(h[h21][:,None], h[h22][:,:,None])).squeeze(-1)

                combs.append(jetProj)

        out = torch.cat(combs, dim=1)

        return out
    

class pairAGraph(nn.Module):     
    def __init__(self,inpt_dim=4,embed_dim=10,ff_dim=10,nLayers=1,nHeads=1,p=0.1,njets=4,
                 scoreFct='jetCompatibility',preLN=False):
        '''
        Inputs:
        - in_dim: # of features used to repn each jet
        - embed_dim: size of the latent space representing the jets
        - nAttnBlocks: # of attn blocks to use before classifying 
                       the edges
        - nHeads:
        - p: dropout fraction
        - njets: Max # of jets to include
        - scoreFct: The model to use for defining the score 
            * jetCompatibility:
            * edgeMax:
            * minL2:
            * gaussianSimilarity:
            * tDist:
        '''
        #super(torch.jit.ScriptModule, self).__init__()
        super(pairAGraph, self).__init__()
    
        self.toLatent = nn.Linear(inpt_dim,embed_dim,bias=True)
        self.relu = nn.ReLU()

        #self.encoderLayers = _get_clones(nn.TransformerEncoderLayer(*params), nAttnBlocks)
        params = (embed_dim, nHeads, ff_dim, p)
        
        if preLN:
            print('pre-LayerNorm transformer')
            self.encoderLayer = TransformerPreLNEncoderLayer(*params) 
            self.finalNorm = nn.LayerNorm(embed_dim)
        else:
            print('post-LayerNorm transformer')
            self.encoderLayer = nn.TransformerEncoderLayer(*params)
            self.finalNorm = lambda x: x # just the idenity function 
        
        assert nLayers == 1 # I've only added in functionality for 2 layers so far

        self.finalAttnLayer = nn.MultiheadAttention(embed_dim, nHeads)
        
        self.embed_dim = embed_dim
        self.njets=njets

        print(self.embed_dim,type(self.embed_dim))
    
        if scoreFct == 'jetCompatibility':
            #self.score = jetCompatibility(njets)

            self.score = lambda h,e: \
                torch.cat([(torch.bmm(h[h11][:,None], h[h12][:,:,None]) \
                            + torch.bmm(h[h21][:,None], h[h22][:,:,None])).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1)
        elif scoreFct == 'edgeMax':
            self.score = lambda h,e: \
                torch.cat([(e[:,h11,h12] + e[:,h12,h21] + e[:,h21,h22] + e[:,h22,h21]).view(-1,1)  
                          for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                          for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)])],axis=1)
        elif scoreFct == 'minL2':
            self.score = lambda h,e: \
                torch.cat([(torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1)) \
                           + torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h11].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h12].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h21].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h22].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           ).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1)
        elif scoreFct == 'gaussianSimilarity':
            self.score = lambda h,e: \
                torch.exp(torch.cat([(torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1)) \
                           + torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h11].view(-1,1,self.embed_dim), h[h11].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h12].view(-1,1,self.embed_dim), h[h12].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h21].view(-1,1,self.embed_dim), h[h21].view(-1,self.embed_dim,1))
                           - 0.5 * torch.bmm(h[h22].view(-1,1,self.embed_dim), h[h22].view(-1,self.embed_dim,1))
                           ).squeeze(-1)
                           for i0,i1,i2,i3 in combinations(range(self.njets),4) \
                           for (h11,h12), (h21, h22) in zip([(i0,i1),(i0,i2),(i0,i3)],[(i2,i3),(i1,i3),(i1,i2)]) ],
                          axis=1))
        else:
            print(f'Error: {scoreFct} not a recognized input for scoreFct - setting score to 0')
            self.score = lambda h: 0 

    # def jetCompatibility(self, h,e: Optional[torch.Tensor]=None):
    # 
    #         njets = h.shape[0]
    #         combs = []
    #         for i in torch.combinations(torch.arange(njets),4): 
    #             for (h11,h12), (h21, h22) in zip([(i[0],i[1]),(i[0],i[2]), (i[0],i[3])],
    #                                              [(i[2],i[3]),(i[1],i[3]),(i[1],i[2])]):
    #                 jetProj = (torch.bmm(h[h11][:,None], h[h12][:,:,None]) \
    #                            + torch.bmm(h[h21][:,None], h[h22][:,:,None])).squeeze(-1)
    #                 combs.append(jetProj)
    # 
    #         out = torch.cat(combs, dim=1)
    #         return out

    def forward(self, x):
        '''
        Inputs:
        - x has shape (batchSize, nJets, nFeatures)
        '''
    
        self.njets = x.shape[1]
    
        xi = x.permute(1,0,2) 
        h = self.toLatent(xi)
        
        #h = nn.ReLU()(h)
        h = self.relu(h)
        
        h = self.encoderLayer(h)
        h = self.finalNorm(h)
        h, e = self.finalAttnLayer(h,h,h)

        score = self.score(h,e)

        return score, e
